# Do This

A set of ESLint rules to make your code better.

## Included Rules

### `no-inhuman-const`
The only value of higher-order programming languages is clearly communicating with other developers.
Only `string`s and `number`s are actually constant values, and are the only values that should be assigned to `const` when it's instantiated.

Read [Stop Writing Inhuman const](https://log.rockerest.com/post/stop-writing-inhuman-const.html).

### `no-multiple-exit`
Functions should have a single entry point and a single exit point. More than one exit point is a recipe for buggy edge cases and untraceable code execution paths.

### `uppercase-const`
A common pattern is to use an upper-, snake-case name for constants. This makes them easy to pick out, and most syntax highlighters will provide a special color

### `cyclomatic-complexity`
The default `complexity` rule in ESLint includes rules that do not affect the number of linearly independent code paths. For example: default values for parameters. This rule only counts linearly independent code paths to increase complexity.

# Install

`npm install eslint-plugin-do-this`

# Use

Add the plugin to your ESLint configuration:

```
import doThis from "eslint-plugin-do-this";

// ...

"plugins": [
    "do-this": doThis
]
```

Then, enable the rules you want:

```
"rules": {
    "do-this/no-inhuman-const": "error",
    "do-this/no-multiple-exit": "error",
    "do-this/uppercase-const": "error",
    "do-this/cyclomatic-complexity": [
        "error",
        {
            "maximum": 3
        }
    ]
}
```
