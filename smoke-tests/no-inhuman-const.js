/* passes */
const ALPHA = 1;
const BETA = "foo";
const GAMMA = false;
const DELTA = Symbol();
const ECHO = undefined;
const SIGMA = null;
const OMEGA = `foo`;

/* fails */
const FOO = {};
const BAR = [];
const BAZ = () => {};
const BAP = test`things`;
const BIM = stuff();
for( const X of [] ){}