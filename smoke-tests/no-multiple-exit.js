/* passes */
() => {
	var doThing = ( foo ) => {
		return foo;
	}

	return doThing();
}

() => {
	function foo(){
		return "foo";
	}

	return foo();
}

() => {
	if( foo ){
		foo.map( ( f ) => {
			return f;
		} );
	}
}

var bagOfFns = {
	foo(){
		return "bar";
	},
	"bar": function(){
		return "baz";
	},
	"baz": () => {
		return "bap";
	}
};

( {
	foo(){
		return "bar";
	},
	"bar": function(){
		return "baz";
	},
	"baz": () => {
		return "bap";
	}
} );

function f(){
	return 1;
}
function g(){
	var multi = 0;

	if( Math.round( Math.random() ) ){
		multi = 1;
	}

	return multi;
}
function block(){
	var result = 1;

	{
		let internal = result;

		return internal;
	}
}

/* fails */
function h(){
	if( Math.round( Math.random() ) ){
		return 1;
	}
	else{
		return 0;
	}
}
function i(){
	switch( Math.round( Math.random() ) ){
		case 0:
			return "zero";
			break;
		case 1:
			return "one";
			break;
	}
}
var bagOfFns = {
	foo(){
		if( Math.round( Math.random() ) ){
			return "bar";
		}
		else{
			return "other";
		}
	},
	"bar": function(){
		if( Math.round( Math.random() ) ){
			return "baz";
		}
		else{
			return "other";
		}
	},
	"baz": () => {
		if( Math.round( Math.random() ) ){
			return "bap";
		}
		else{
			return "other";
		}
	}
};

( {
	foo(){
		if( Math.round( Math.random() ) ){
			return "bar";
		}
		else{
			return "other";
		}
	},
	"bar": function(){
		if( Math.round( Math.random() ) ){
			return "baz";
		}
		else{
			return "other";
		}
	},
	"baz": () => {
		if( Math.round( Math.random() ) ){
			return "bap";
		}
		else{
			return "other";
		}
	}
} );
function forTest( foo ){
	for( x in foo ){
		return x;
	}

	for( y of foo ){
		return y;
	}

	for( let i = 0; i++; i < 10 ){
		return i;
	}
}
function catchTest(){
	try{
		thrower();
	}
	catch( e ){
		return e;
	}
}