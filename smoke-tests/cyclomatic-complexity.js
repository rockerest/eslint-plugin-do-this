/* passes */
() => {
    for( let x of 10 ){
        console.log( x ? "a" : "b" );
    }
}

/* fails */
() => {
    for( let x of 10 ){
        console.log( x ? "a" : "b" );
    }

    return Math.random() > 0.5 ? "true" : "false";
}