import doThis from "eslint-plugin-do-this";

export default {
	"languageOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": {
        "do-this": doThis
    },
	"rules": {
		"do-this/no-inhuman-const": "error",
		"do-this/no-multiple-exit": "error",
		"do-this/uppercase-const": "error",
        "do-this/cyclomatic-complexity": [
            "error",
            {
                "maximum": 3
            }
        ]
	}
};