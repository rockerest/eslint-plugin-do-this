function getAncestors( node ){
	var tree = [];

	while( node.parent ){
		tree.unshift( node.parent );

		node = node.parent;
	}

	return tree;
}

function getClosestFunctionNode( tree ){
	var bottomUpTree = tree.reverse();

	return bottomUpTree
		.find(
			( node ) => [
				"FunctionDeclaration",
				"FunctionExpression",
				"ArrowFunctionExpression"
			].includes( node.type )
		);
}

function getPositionOfNode( tree, node ){
	return tree.findIndex( ( ancestor ) => ancestor == node );
}

function getReturnStatementsBeforeNodeInScope( node, scope, context ){
	var source = context.sourceCode;

	return source
		.getTokensBetween( source.getFirstToken( scope ), node )
		.filter( ( token ) => token.type == "Keyword" && token.value == "return" )
		.map( ( token ) => source.getNodeByRangeIndex( token.range[ 0 ] ) );
}

function returnInScope( returnNode, scopeNode ){
	var returnScope = getClosestFunctionNode( getAncestors( returnNode ) );

	return returnScope.range[ 0 ] == scopeNode.range[ 0 ];
}

function hasDisallowedAncestor( ancestors ){
	var enclosingScope = getClosestFunctionNode( ancestors );
	var disallowedAncestors = new Set( [
		"IfStatement",
		"SwitchStatement",
		"SwitchCase",
		"ForStatement",
		"ForInStatement",
		"ForOfStatement",
		"CatchClause"
	] );
	var types = {
		"IfStatement": "conditional",
		"SwitchStatement": "conditional",
		"SwitchCase": "conditional",
		"ForStatement": "loop",
		"ForInStatement": "loop",
		"ForOfStatement": "loop",
		"CatchClause": "clause"
	};
	var disallowed = ancestors.reverse().find( ( node ) => disallowedAncestors.has( node.type ) );
	var type = disallowed ? types[ disallowed.type ] : undefined;

	if( disallowed && getPositionOfNode( ancestors, enclosingScope ) > getPositionOfNode( ancestors, disallowed ) ){
		disallowed = false;
		type = undefined;
	}

	return [ Boolean( disallowed ), type ];
}

function oneOfMultiple( node, context ){
	var tree = context.sourceCode.getAncestors( node );
	var fnNode = getClosestFunctionNode( tree );
	// otherReturns is a list of all `return` statements before the return we're
	// looking at now (`node`) but after its function scope
	var otherReturns = getReturnStatementsBeforeNodeInScope( node, fnNode, context );
	// otherReturnsInSameScope eliminates any returns from the above list that have
	// an intermediate function scope
	var otherReturnsInSameScope = otherReturns.filter( ( returnNode ) => returnInScope( returnNode, fnNode ) );

	return otherReturnsInSameScope.length > 0;
}

export default {
	"meta": {
		"schema": [ {
			"enum": ["always", "never"]
		} ]
	},
	create: function( context ) {
		return {
			"ReturnStatement:exit": function( node ) {
				var tree = context.sourceCode.getAncestors( node );
				var messages = {
					"multiple": "Only one exit point is allowed.",
					"conditional": "Exit points are not allowed in conditionals.",
					"loop": "Exit points are not allowed in loops.",
					"clause": "Exit points are not allowed in conditional clauses."
				}
				var disallowed;
				var type;

				if( oneOfMultiple( node, context ) ){
					context.report( {
						"message": messages.multiple,
						"node": node
					} );
				}
				else{
					[ disallowed, type ] = hasDisallowedAncestor( tree )
					
					if( disallowed ){
						context.report( {
							"message": messages[ type ],
							"node": node
						} );
					}
				}
			}
		};
	}
}