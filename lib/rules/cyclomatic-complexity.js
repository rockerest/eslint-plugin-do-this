import {
    isLogicalAssignmentOperator,
    getFunctionNameWithKind
} from "../eslint-ast.js";

function ucFirst( str ){
    var uc;

    if( str.length <= 1 ){
        uc = str.toUpperCase();
    }
    else{
        uc = str[ 0 ].toUpperCase() + str.slice( 1 );
    }

    return uc;
}

export default {
    "meta": {
        "schema": [
            {
                "oneOf": [
                    {
                        "type": "integer",
                        "minimum": 0
                    },
                    {
                        "type": "object",
                        "properties": {
                            "maximum": {
                                "type": "integer",
                                "minimum": 0
                            },
                            "max": {
                                "type": "integer",
                                "minimum": 0
                            }
                        },
                        "additionalProperties": false
                    }
                ]
            }
        ],
        "messages": {
            "complex": "{{name}} has a complexity of {{complexity}}. Maximum allowed is {{max}}."
        }
    },
    create( context ){
        const DEFAULT_THRESHOLD = 10;
        var threshold = DEFAULT_THRESHOLD;
        var option = context.options[0];
        var hasMax = typeof option == "object" && ( Object.hasOwn(option, "maximum") || Object.hasOwn(option, "max") );
        var complexities = [];

        if( hasMax ){
            threshold = option.maximum || option.max;
        }
        else if( typeof option === "number" ){
            threshold = option;
        }

        function increaseComplexity(){
            complexities[ complexities.length - 1 ]++;
        }

        return {

            onCodePathStart() {
                complexities.push( 1 );
            },

            CatchClause: increaseComplexity,
            ConditionalExpression: increaseComplexity,
            LogicalExpression: increaseComplexity,
            ForStatement: increaseComplexity,
            ForInStatement: increaseComplexity,
            ForOfStatement: increaseComplexity,
            IfStatement: increaseComplexity,
            WhileStatement: increaseComplexity,
            DoWhileStatement: increaseComplexity,

            // Avoid `default`
            "SwitchCase[test]": increaseComplexity,

            // Logical assignment operators have short-circuiting behavior
            AssignmentExpression( node ){
                if( isLogicalAssignmentOperator( node.operator ) ){
                    increaseComplexity();
                }
            },

            MemberExpression( node ){
                if( node.optional == true ){
                    increaseComplexity();
                }
            },

            CallExpression( node ){
                if( node.optional == true ){
                    increaseComplexity();
                }
            },

            onCodePathEnd( codePath, node ){
                var complexity = complexities.pop();
                var orig = codePath.origin;
                var name;

                if(
                    complexity > threshold &&
                    [ "function", "class-field-initializer", "class-static-block" ].includes( orig )
                ){
                    if( orig == "class-field-initializer" ){
                        name = "class field initializer";
                    }
                    else if( orig == "class-static-block" ){
                        name = "class static block";
                    }
                    else{
                        name = getFunctionNameWithKind( node );
                    }

                    context.report( {
                        node,
                        messageId: "complex",
                        data: {
                            name: ucFirst( name ),
                            complexity,
                            max: threshold
                        }
                    } );
                }
            }
        };
    }
};