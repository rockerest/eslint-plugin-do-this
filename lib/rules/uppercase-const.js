export default {
	"meta": {
		"schema": [ {
			"enum": ["always", "never"]
		} ]
	},
	create: function( context ) {
		return {
			"VariableDeclaration:exit": function( node ) {
				if( node.kind == "const" ){
					node
						.declarations
						.forEach( ( declaration ) => {
							if( declaration.id.name != String( declaration.id.name ).toUpperCase() ){
								context.report( {
									"message": "const declaration variable names should be uppercase.",
									"loc": declaration.id.loc
								} );
							}
						} );
				}
			}
		};
	}
}