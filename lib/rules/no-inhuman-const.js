function getErrorMessage( declarationNode ){
	var type = declarationNode.init?.type;
	var messages = {
		"ObjectExpression": "Objects are not constants.",
		"ArrayExpression": "Arrays are not constants.",
		"FunctionExpression": "Functions are not constants.",
		"ArrowFunctionExpression": "Functions are not constants.",
		"CallExpression": "The results of a call to a function are not constant.",
		"TaggedTemplateExpression": "The results of a call to a function (even a template tag function) are not constant."
	};

	return messages[ type ] || "Only primitives can be constants.";
}

function getSpecialErrorMessage( specialCase ){
	var messages = {
		"ForOfStatement": "Variables whose assignment is an iterable step value are not constants."
	}

	return messages[ specialCase ];
}

function checkSpecialType( declaration, context ){
	var types = {
		"CallExpression": ( decl, ctx ) => {
			if( decl.init.callee.name != "Symbol" ){
				ctx.report( {
					"message": getErrorMessage( decl ),
					"node": decl
				} );
			}
		},
		"undefined": ( decl, ctx ) => {
			if( decl.parent/*VariableDeclaration*/.parent/*ForOfStatement*/.type == "ForOfStatement" ){
				ctx.report( {
					"message": getSpecialErrorMessage( "ForOfStatement" ),
					"node": decl
				} );
			}
		}
	}

	if( types[ declaration.init?.type ] ){
		types[ declaration.init?.type ]( declaration, context );
	}
}

function checkDeclarator( declaration, context ){
	var type = declaration.init?.type;
	var allowedTypes = new Set( [
		"Literal",
		"TemplateLiteral",
		"Identifier",
		"UnaryExpression",
		"BinaryExpression"
	] );
	var specialTypes = new Set( [
		"CallExpression",
		undefined
	] );

	if( !allowedTypes.has( type ) && !specialTypes.has( type ) ){
		context.report( {
			"message": getErrorMessage( declaration ),
			"node": declaration
		} );
	}
	else{
		if( specialTypes.has( type ) ){
			checkSpecialType( declaration, context );
		}
	}
}

export default {
	"meta": {
		"schema": [ {
			"enum": ["always", "never"]
		} ]
	},
	create: function( context ) {
		return {
			"VariableDeclaration:exit": function( node ) {
				if( node.kind == "const" ){
					node
						.declarations
						.forEach( ( declaration ) => checkDeclarator( declaration, context ) );
				}
			}
		};
	}
}