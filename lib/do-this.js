import inhumanConst from "./rules/no-inhuman-const.js";
import multiExit from "./rules/no-multiple-exit.js";
import upperConst from "./rules/uppercase-const.js";
import cycloComplex from "./rules/cyclomatic-complexity.js";

export default {
    rules: {
        "no-inhuman-const": inhumanConst,
        "no-multiple-exit": multiExit,
        "uppercase-const": upperConst,
        "cyclomatic-complexity": cycloComplex
    }
};
