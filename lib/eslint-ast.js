// From: https://github.com/eslint/eslint/blob/main/lib/rules/utils/ast-utils.js

const LOGICAL_ASSIGNMENT_OPERATORS = new Set( [ "&&=", "||=", "??=" ] );

function getStaticStringValue(node) {
    switch (node.type) {
        case "Literal":
            if (node.value === null) {
                if (isNullLiteral(node)) {
                    return String(node.value); // "null"
                }
                if (node.regex) {
                    return `/${node.regex.pattern}/${node.regex.flags}`;
                }
                if (node.bigint) {
                    return node.bigint;
                }

                // Otherwise, this is an unknown literal. The function will return null.

            } else {
                return String(node.value);
            }
            break;
        case "TemplateLiteral":
            if (node.expressions.length === 0 && node.quasis.length === 1) {
                return node.quasis[0].value.cooked;
            }
            break;

            // no default
    }

    return null;
}

function getStaticPropertyName(node) {
    let prop;

    switch (node && node.type) {
        case "ChainExpression":
            return getStaticPropertyName(node.expression);

        case "Property":
        case "PropertyDefinition":
        case "MethodDefinition":
            prop = node.key;
            break;

        case "MemberExpression":
            prop = node.property;
            break;

            // no default
    }

    if (prop) {
        if (prop.type === "Identifier" && !node.computed) {
            return prop.name;
        }

        return getStaticStringValue(prop);
    }

    return null;
}

export function isLogicalAssignmentOperator( operator ){
    return LOGICAL_ASSIGNMENT_OPERATORS.has( operator );
}

export function getFunctionNameWithKind( node ){
    const parent = node.parent;
    const tokens = [];

    if (parent.type === "MethodDefinition" || parent.type === "PropertyDefinition") {

        // The proposal uses `static` word consistently before visibility words: https://github.com/tc39/proposal-static-class-features
        if (parent.static) {
            tokens.push("static");
        }
        if (!parent.computed && parent.key.type === "PrivateIdentifier") {
            tokens.push("private");
        }
    }
    if (node.async) {
        tokens.push("async");
    }
    if (node.generator) {
        tokens.push("generator");
    }

    if (parent.type === "Property" || parent.type === "MethodDefinition") {
        if (parent.kind === "constructor") {
            return "constructor";
        }
        if (parent.kind === "get") {
            tokens.push("getter");
        } else if (parent.kind === "set") {
            tokens.push("setter");
        } else {
            tokens.push("method");
        }
    } else if (parent.type === "PropertyDefinition") {
        tokens.push("method");
    } else {
        if (node.type === "ArrowFunctionExpression") {
            tokens.push("arrow");
        }
        tokens.push("function");
    }

    if (parent.type === "Property" || parent.type === "MethodDefinition" || parent.type === "PropertyDefinition") {
        if (!parent.computed && parent.key.type === "PrivateIdentifier") {
            tokens.push(`#${parent.key.name}`);
        } else {
            const name = getStaticPropertyName(parent);

            if (name !== null) {
                tokens.push(`'${name}'`);
            } else if (node.id) {
                tokens.push(`'${node.id.name}'`);
            }
        }
    } else if (node.id) {
        tokens.push(`'${node.id.name}'`);
    }

    return tokens.join(" ");
}