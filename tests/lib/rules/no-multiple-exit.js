import { RuleTester } from "eslint";

import multiExit from "../../../lib/rules/no-multiple-exit.js";

var tester = new RuleTester();

tester.run( "no-multiple-exit", multiExit, {
	"valid": [
		{
			code: `() =>{
				return 1;
			}`
		},
		{
			code: `() => {
				var result = 0;

				if( foo ){
					result = 1;
				}

				return result;
			}`
		},
		{
			code: `() => {
				var result = 0;

				{
					let a = result;

					return a;
				}
			}`
		},
		{
			code: `() => {
				var doThing = ( foo ) => {
					return foo;
				}
			
				return doThing();
			}`
		},
		{
			code: `() => {
				function foo(){
					return "foo";
				}
			
				return foo();
			}`
		},
		{
			code: `() => {
				if( foo ){
					foo.map( ( f ) => {
						return f;
					} );
				}
			}`
		}
	],
	"invalid": [
		{
			code: `() => {
				if( foo ){
					return 1;
				}
				else{
					return 2;
				}
			}`,
			errors: [
				{ message: "Exit points are not allowed in conditionals." },
				{ message: "Only one exit point is allowed." }
			]
		},
		{
			code: `() => {
				switch( Math.round( Math.random() ) ){
					case 0:
						return "zero";
						break;
					case 1:
						return "one";
						break;
				}
			}`,
			errors: [
				{ message: "Exit points are not allowed in conditionals." },
				{ message: "Only one exit point is allowed." }
			]
		},
		{
			code: `var bagOfFns = {
				foo(){
					if( Math.round( Math.random() ) ){
						return "bar";
					}
					else{
						return "other";
					}
				},
				"bar": function(){
					if( Math.round( Math.random() ) ){
						return "baz";
					}
					else{
						return "other";
					}
				},
				"baz": () => {
					if( Math.round( Math.random() ) ){
						return "bap";
					}
					else{
						return "other";
					}
				}
			};`,
			errors: [
				{ message: "Exit points are not allowed in conditionals." },
				{ message: "Only one exit point is allowed." },
				{ message: "Exit points are not allowed in conditionals." },
				{ message: "Only one exit point is allowed." },
				{ message: "Exit points are not allowed in conditionals." },
				{ message: "Only one exit point is allowed." }
			]
		},
		{
			code: `( {
				foo(){
					if( Math.round( Math.random() ) ){
						return "bar";
					}
					else{
						return "other";
					}
				},
				"bar": function(){
					if( Math.round( Math.random() ) ){
						return "baz";
					}
					else{
						return "other";
					}
				},
				"baz": () => {
					if( Math.round( Math.random() ) ){
						return "bap";
					}
					else{
						return "other";
					}
				}
			} );`,
			errors: [
				{ message: "Exit points are not allowed in conditionals." },
				{ message: "Only one exit point is allowed." },
				{ message: "Exit points are not allowed in conditionals." },
				{ message: "Only one exit point is allowed." },
				{ message: "Exit points are not allowed in conditionals." },
				{ message: "Only one exit point is allowed." }
			]
		},
		{
			code: `function forTest( foo ){
				for( x in foo ){
					return x;
				}
			
				for( y of foo ){
					return y;
				}
			
				for( let i = 0; i++; i < 10 ){
					return i;
				}
			}`,
			errors: [
				{ message: "Exit points are not allowed in loops." },
				{ message: "Only one exit point is allowed." },
				{ message: "Only one exit point is allowed." }
			]
		},
		{
			code: `function catchTest(){
				try{
					thrower();
				}
				catch( e ){
					return e;
				}
			}`,
			errors: [{ message: "Exit points are not allowed in conditional clauses." }]
		}
	]
});