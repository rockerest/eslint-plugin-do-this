import { RuleTester } from "eslint";

import inhumanConst from "../../../lib/rules/no-inhuman-const.js";

var tester = new RuleTester();

tester.run( "no-inhuman-const", inhumanConst, {
	"valid": [
		{
			code: 'const STRING = "string"'
		},
		{
			code: "const STRING = 'string'"
		},
		{
			code: "const STRING = `STRING`"
		},
		{
			code: "const BOOL = true"
		},
		{
			code: "const BOOL = false"
		},
		{
			code: "const NUM = 1"
		},
		{
			code: "const NUM = 1e2"
		},
		{
			code: "const NUM = 1.2"
		},
		{
			code: "const NUM = -1"
		},
		{
			code: "const NUM = +1"
		},
		{
			code: "const NUM = 1 + 5"
		},
		{
			code: "const NUM = ( 1 + 5 )"
		},
		{
			code: "const NULL = null"
		},
		{
			code: "const UNDEFINED = undefined"
		},
		{
			code: "const SYMBOL = Symbol()"
		}
	],
	"invalid": [
		{
			code: "const OBJ = {}",
			errors: [{ message: "Objects are not constants." }]
		},
		{
			code: "const ARRAY = []",
			errors: [{ message: "Arrays are not constants." }]
		},
		{
			code: "const FN = function fn(){}",
			errors: [{ message: "Functions are not constants." }]
		},
		{
			code: "const FN = function (){}",
			errors: [{ message: "Functions are not constants." }]
		},
		{
			code: "const FN = () => {}",
			errors: [{ message: "Functions are not constants." }]
		},
		{
			code: "const RESULT = something()",
			errors: [{ message: "The results of a call to a function are not constant." }]
		},
		{
			code: "const RESULT = tag``",
			errors: [{ message: "The results of a call to a function (even a template tag function) are not constant." }]
		}
	]
});