import { RuleTester } from "eslint";

import upperConst from "../../../lib/rules/uppercase-const.js";

var tester = new RuleTester();

tester.run( "uppercase-const", upperConst, {
	"valid": [
		{
			code: 'const FOO = "string"'
		}
	],
	"invalid": [
		{
			code: 'const foo = "stuff"',
			errors: [{ message: "const declaration variable names should be uppercase." }]
		}
	]
});