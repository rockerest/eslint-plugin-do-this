import { fileURLToPath } from 'url';
import { exec } from "child_process";

var __dirname = fileURLToPath( new URL( ".", import.meta.url ) );
var execHandler = ( err, stdOut, stdErr ) => {
	if( err ){
		console.error( err );
	}
	else{
		console.log( stdOut );
		console.log( stdErr );
	}
}

exec( `node ${__dirname}lib/rules/no-inhuman-const.js`, execHandler );
exec( `node ${__dirname}lib/rules/no-multiple-exit.js`, execHandler );
exec( `node ${__dirname}lib/rules/uppercase-const.js`, execHandler );
exec( `node ${__dirname}lib/rules/cyclomatic-complexity.js`, execHandler );